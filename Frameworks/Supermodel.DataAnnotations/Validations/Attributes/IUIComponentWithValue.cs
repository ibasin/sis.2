﻿#nullable enable

namespace Supermodel.DataAnnotations.Validations.Attributes
{
    public interface IUIComponentWithValue
    {
        string ComponentValue { get; set; }
    }
}
