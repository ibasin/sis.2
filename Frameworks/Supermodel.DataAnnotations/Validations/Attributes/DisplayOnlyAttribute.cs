﻿#nullable enable

using System;

namespace Supermodel.DataAnnotations.Validations.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayOnlyAttribute : Attribute { }
}