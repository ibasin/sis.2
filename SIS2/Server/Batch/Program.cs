#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Supermodel.Persistence.EFCore;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.ReflectionMapper;

namespace Batch
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();

            //Comment this out if you don't want to recreate and re-seed db every time you start the app in debug mode
            if (Debugger.IsAttached)
            {
                Console.Write("Recreating the database... ");
                await using (new UnitOfWork<DataContext>())
                {
                    await EFCoreUnitOfWorkContext.Database.EnsureDeletedAsync();
                    await EFCoreUnitOfWorkContext.Database.EnsureCreatedAsync();
                    await UnitOfWorkContext.SeedDataAsync();
                }
                Console.WriteLine("Done!");
            }

            //await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            //{
            //    var repo = LinqRepoFactory.Create<Student>();
            //    await repo.Items.Where(x => x.Gender == GenderEnum.Male).DeleteAsync();
            //}

            //await using (new UnitOfWork<DataContext>())
            //{
            //    var repo = LinqRepoFactory.Create<Student>();
            //    //much much better way of querying data 
            //    var maleStudents = await repo.Items.Where(x => x.Gender == GenderEnum.Male).ToListAsync();
            //    foreach (var maleStudent in maleStudents)
            //    {
            //        maleStudent.Delete();
            //    }
            //}

            //await using (new UnitOfWork<DataContext>())
            //{
            //    var majors = await RepoFactory.Create<Major>().GetAllAsync();

            //    var whatley = new Student { SocialSecurityNumber = "555-55-5555", FirstName = "Tim", LastName = "Whatley", Gender = GenderEnum.Male, Major = majors.Single(x => x.Name == "Math") };
            //    whatley.Add();

            //    var newman = new Student { SocialSecurityNumber = "666-66-6666", FirstName = "Norman", LastName = "Newman", Gender = GenderEnum.Male, Major = majors.Single(x => x.Name == "English") };
            //    newman.Add();

            //    var seinfeld = await LinqRepoFactory.Create<Student>().Items.SingleAsync(x => x.SocialSecurityNumber == "111-11-1111");
            //    seinfeld.SocialSecurityNumber = "000-00-0000";

            //    var costanza = await LinqRepoFactory.Create<Student>().Items.SingleAsync(x => x.SocialSecurityNumber == "222-22-2222");
            //    costanza.Delete();

            //    //UnitOfWorkContext.CommitOnDispose = false;
            //}

            //await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            //{
            //    var repo = RepoFactory.Create<Student>();
            //    var students = await repo.GetAllAsync();
            //    //very bad way to query data
            //    var maleStudents = students.Where(x => x.Gender == GenderEnum.Male).ToList();
            //    PrintStudents(maleStudents);
            //}

            //Console.WriteLine();

            //await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            //{
            //    var repo = LinqRepoFactory.Create<Student>();
            //    //much much better way of querying data
            //    var maleStudents = await repo.Items.Where(x => x.Gender == GenderEnum.Male).ToListAsync();
            //    PrintStudents(maleStudents);
            //}

            //Console.WriteLine();

            //await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            //{
            //    var repo = new StudentRepo(); //(StudentRepo)RepoFactory.Create<Student>();
            //    //get all male students using a custom repo 
            //    var maleStudents = await repo.GetByGenderAsync(GenderEnum.Male);
            //    PrintStudents(maleStudents);
            //}
        }

        private static void PrintStudents(List<Student> students)
        {
            foreach (var student in students)
            {
                Console.WriteLine($"{student.SocialSecurityNumber}, {student.FirstName} {student.LastName}, {student.Gender.GetDescription()}, {student.Major.Name}");
            }
        }
    }
}
