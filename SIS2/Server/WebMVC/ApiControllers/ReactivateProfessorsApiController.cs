﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Controllers.Api;

namespace WebMVC.ApiControllers
{
    [Authorize]
    public class ReactivateProfessorsApiController 
        : CommandApiController<ReactivateProfessorsApiController.Input, ReactivateProfessorsApiController.Output>
    {
        #region Embedded Types
        public class Input
        {
            #region Properties
            public string Suffix { get; set; } = "";
            #endregion
        }
        public class Output
        {
            #region Properties
            public int ReactivatedCount { get; set; }
            #endregion
        }
        #endregion

        #region Overrides
        protected override async Task<Output> ExecuteAsync(Input input)
        {
            var output = new Output();

            await using(new UnitOfWork<DataContext>())
            {
                var inactiveProfessors = LinqRepoFactory.Create<Professor>().Items.Where(x => !x.Active).ToArray();

                foreach (var professor in inactiveProfessors)
                {
                    professor.Active = true;
                    professor.LastName += input.Suffix; 
                }

                output.ReactivatedCount = inactiveProfessors.Length;
            }

            return output;
        }
        #endregion
    }
}
