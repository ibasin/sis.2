﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Supermodel.Presentation.Mvc.Controllers.Api;

namespace WebMVC.ApiControllers
{
    [Authorize]
    public class ProfessorLookupApiController : AutocompleteApiController<Professor, DataContext>
    {
        #region Overrides
        protected override async Task<List<string>> AutocompleteAsync(IQueryable<Professor> items, string term)
        {
            var lowerCaseTerm = term.ToLower();
            var result = await items
                .Where(x => x.Active && x.FullName.ToLower().Contains(lowerCaseTerm))
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .Select(x => $"{x.FirstName} {x.LastName}")
                .Take(25)
                .ToListAsync();

            return result;
        }
        #endregion
    }
}
