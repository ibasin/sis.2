﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Supermodel.Presentation.Mvc.Auth;
using Supermodel.Presentation.Mvc.Controllers.Api;
using Supermodel.Presentation.Mvc.Models.Api;
using WebMVC.Models;

namespace WebMVC.ApiControllers
{
    [Authorize]
    public class DepartmentApiController 
        : EnhancedCRUDApiController<Department, DepartmentApiModel, SimpleSearchApiModel, DataContext> 
    { 
        #region Overrides
        protected override IQueryable<Department> ApplySearchBy(IQueryable<Department> items, SimpleSearchApiModel searchBy)
        {
            if (!string.IsNullOrEmpty(searchBy.SearchTerm)) items = items.Where(x => x.Name == searchBy.SearchTerm);
            
            return items;
        }
        protected override async Task<ValidateLoginResponseApiModel> GetValidateLoginResponseAsync()
        {
            var result =  await base.GetValidateLoginResponseAsync();
            result.UserLabel = UserHelper.GetCurrentUserLabel();
            return result;
        }
        #endregion
    }
}
