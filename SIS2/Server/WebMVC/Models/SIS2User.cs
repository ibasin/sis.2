#nullable enable

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Entities;
using Supermodel.DataAnnotations.Validations;
using Supermodel.DataAnnotations.Validations.Attributes;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Models.Api;
using Supermodel.ReflectionMapper;

namespace WebMVC.Models
{
    public class SIS2UserUpdatePasswordApiModel : ApiModelForEntity<SIS2User>
    {
        #region Overrides
        public override Task<T> MapToCustomAsync<T>(T other)
        {
            var user = CastToEntity(other);
            user.Password = NewPassword;
            return base.MapToCustomAsync(other);
        }
        public override Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            var vr = new ValidationResultList();
            return Task.FromResult(vr);
        }
        #endregion
        
        #region Properties
        [Required, NotRMapped] public string OldPassword { get; set; } = "";

        [Required, NotRMapped]
        public string NewPassword { get; set; } = "";
        #endregion
    }    
    
    public class SIS2UserUpdatePasswordMvcModel : Bs4.MvcModelForEntity<SIS2User>
    {
        #region Overrides
        public override string Label => "N/A";
        public override Task<T> MapToCustomAsync<T>(T other)
        {
            var user = CastToEntity(other);
            user.Password = NewPassword.Value;
            return base.MapToCustomAsync(other);
        }
        public override Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            var vr = new ValidationResultList();
            return Task.FromResult(vr);
        }
        #endregion

        #region Properties
        [Required, NotRMapped] public Bs4.PasswordTextBoxMvcModel OldPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };

        [Required, NotRMapped, MustEqualTo(nameof(ConfirmPassword), ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel NewPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };

        [Required, NotRMapped, MustEqualTo(nameof(NewPassword), ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel ConfirmPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };
        #endregion
    }

    public class SIS2UserMvcModel : Bs4.MvcModelForEntity<SIS2User>
    {
        #region Overrides
        public override NumberOfColumnsEnum NumberOfColumns => NumberOfColumnsEnum.Three;
        public override string Label => $"{FirstName} {LastName}";
        public override Task<T> MapToCustomAsync<T>(T other)
        {
            var user = CastToEntity(other);
            if (!string.IsNullOrEmpty(NewPassword.Value)) user.Password = NewPassword.Value;
            return base.MapToCustomAsync(other);
        }
        //public override Task MapFromCustomAsync<T>(T other)
        //{
        //    var sis2User = CastToEntity(other);

        //    if (sis2User.ToDoItems.Any())
        //    {
        //        ToDoAction = new HtmlString($@"
        //            <form method='post' action='DeleteToDoItems/{sis2User.Id}'>
        //                <button type='submit' class='btn btn-danger'>
        //                    <span class='oi oi-x'></span>
        //                    Delete all To Do Items
        //                </button>
        //            </form>
        //        ");
        //    }

        //    return base.MapFromCustomAsync(other);
        //}
        //public override IEntity CreateEntity()
        //{
        //    var entity = (SIS2User)base.CreateEntity();
        //    entity.PasswordHashValue = entity.PasswordHashSalt = "NA";
        //    return entity;
        //}
        #endregion
        
        #region Properties
        [ListColumn(OrderBy = nameof(Username), HeaderOrder = 200), Required] public Bs4.TextBoxMvcModel Username { get; set; } = new Bs4.TextBoxMvcModel();
        [ListColumn(OrderBy = nameof(FirstName)), Required] public Bs4.TextBoxMvcModel FirstName { get; set; } = new Bs4.TextBoxMvcModel();
        [ListColumn(OrderBy = "LastName,FirstName"), Required] public Bs4.TextBoxMvcModel LastName { get; set; } = new Bs4.TextBoxMvcModel();
        [ListColumn(OrderBy = nameof(Admin))] public Bs4.CheckboxMvcModel Admin { get; set; } = new Bs4.CheckboxMvcModel();
        
        [ListColumn(OrderBy = nameof(Address))] public Bs4.USAddressMvcModel Address { get; set; } = new Bs4.USAddressMvcModel();

        [ListColumn(OrderBy = "ToDoItem.Count"), NotRMappedTo, RMapTo(".ToDoItems.Count"), ScaffoldColumn(false)] public int ToDoItemsCount { get; set; }

        //[ListColumn, NotRMapped] public HtmlString ToDoAction { get; set; } = HtmlString.Empty;

        [RMapTo(".SIS2UserPicture.*")] public Bs4.BinaryFileMvcModel Picture { get; set; } = new Bs4.BinaryFileMvcModel();
        [ListColumn, RMapTo(".SIS2UserPicture.Picture"), DisplayName(" ")] public Bs4.ImageMvcModel PictureView { get; set; } = new Bs4.ImageMvcModel { HtmlAttributesAsObj = new { height = 35 } };

        [ForceRequiredLabel, NotRMapped, MustEqualTo(nameof(ConfirmPassword), ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel NewPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };

        [ForceRequiredLabel, NotRMapped, MustEqualTo(nameof(NewPassword), ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel ConfirmPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };
        #endregion
    }
}
