﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Supermodel.DataAnnotations.Validations.Attributes;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Models;
using Supermodel.Presentation.Mvc.Models.Api;
using Supermodel.ReflectionMapper;

namespace WebMVC.Models
{
    public class DepartmentApiModel : ApiModelForEntity<Department>
    {
        #region Properties
        [Required] public string Name { get; set; } = "";

        public virtual ListViewModel<MajorApiModel, Major> Majors { get; set; } = new ListViewModel<MajorApiModel, Major>();
        #endregion
    }
    
    public class DepartmentDetailMvcModel : DepartmentListMvcModel
    {
        [NotRMappedTo] public ListViewModel<MajorListMvcModel, Major> Majors { get; set; } = new ListViewModel<MajorListMvcModel, Major>();
    }
    
    public class DepartmentListMvcModel : Bs4.MvcModelForEntity<Department>
    {
        #region Override
        public override string Label => Name.Value;
        #endregion
        
        #region Properties
        [ListColumn(OrderBy = "Name")] public Bs4.TextBoxMvcModel Name { get; set; } = new Bs4.TextBoxMvcModel();
        #endregion
    }
}
