﻿#nullable enable

using System;
using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Supermodel.Persistence.Entities.ValueTypes;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Models.Api;

namespace WebMVC.Models
{
    public class StudentApiModel : ApiModelForEntity<Student>
    {
        #region Properties
        [Required] public string SocialSecurityNumber { get; set; } = "";
        [Required] public string FirstName { get; set; } = "";
        [Required] public string LastName { get; set; } = "";
        public GenderEnum Gender { get; set; }
        public USAddress HomeAddress { get; set; } = new USAddress();
        public USAddress SchoolAddress { get; set; } = new USAddress();
        #endregion
    }
    
    public class StudentLabelMvcModel : Bs4.MvcModelForEntity<Student>
    {
        #region Overrides
        public override string Label => $"{FirstName} {LastName}";
        #endregion
        
        #region Properties
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        #endregion
    }

    public class StudentMvcModel : Bs4.ChildMvcModelForEntity<Student, Major>
    {
        #region Overrides
        public override string Label => $"{FirstName} {LastName}";

        public override Major GetParentEntity(Student entity)
        {
            return entity.Major;
        }
        public override void SetParentEntity(Student entity, Major? parent)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            entity.Major = parent;
        }
        #endregion
        
        #region Properties
        [Required] public Bs4.TextBoxMvcModel SocialSecurityNumber { get; set; } = new Bs4.TextBoxMvcModel();
        [Required] public Bs4.TextBoxMvcModel FirstName { get; set; } = new Bs4.TextBoxMvcModel();
        [Required] public Bs4.TextBoxMvcModel LastName { get; set; } = new Bs4.TextBoxMvcModel();
        [Required] public Bs4.DropdownMvcModelUsingEnum<GenderEnum> Gender { get; set; } = new Bs4.DropdownMvcModelUsingEnum<GenderEnum>();
        public Bs4.USAddressMvcModel HomeAddress { get; set; } = new Bs4.USAddressMvcModel();
        public Bs4.USAddressMvcModel SchoolAddress { get; set; } = new Bs4.USAddressMvcModel();
        public Bs4.CheckboxListMvcModelUsing<ClassLabelMvcModel> ClassesTaken { get; set; } = new Bs4.CheckboxListMvcModelUsing<ClassLabelMvcModel> { Orientation = Orientation.Horizontal };
        #endregion
    }
}
