﻿#nullable enable

using System;
using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Supermodel.DataAnnotations.Validations.Attributes;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Models;
using Supermodel.Presentation.Mvc.Models.Api;
using Supermodel.ReflectionMapper;

namespace WebMVC.Models
{
    public class MajorApiModel : ApiModelForEntity<Major>
    {
        #region Properties
        [Required] public string Name { get; set; } = "";
        [NotRMappedTo] public DateTime? DateCreated { get; set; }
        [NotRMappedTo] public DateTime? DateModified { get; set; }

        public virtual ListViewModel<StudentApiModel, Student> Students { get; set; } = new ListViewModel<StudentApiModel, Student>();
        #endregion
    }
    
    public class MajorDetailMvcModel : MajorListMvcModel
    {
        [NotRMappedTo] public ListViewModel<StudentMvcModel, Student> Students { get; set; } = new ListViewModel<StudentMvcModel, Student>();
    }
    
    public class MajorListMvcModel : Bs4.ChildMvcModelForEntity<Major, Department>
    {
        #region Overrides
        public override string Label => Name.Value;
        
        public override Department GetParentEntity(Major entity)
        {
            return entity.Department;
        }
        public override void SetParentEntity(Major entity, Department? parent)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            entity.Department = parent;
        }

        //public override IEntity CreateEntity()
        //{
        //    var entity = (Major)base.CreateEntity();
        //    entity.DateCreated = entity.DateModified = DateTime.Now;
        //    return entity; 
        //}
        #endregion
        
        #region Properties
        [ListColumn, Required] public Bs4.TextBoxMvcModel Name { get; set; } = new Bs4.TextBoxMvcModel();
        [ListColumn, DisplayOnly, NotRMappedTo] public DateTime? DateCreated { get; set; } 
        [ListColumn, DisplayOnly, NotRMappedTo] public DateTime? DateModified { get; set; } 
        #endregion
    }
}
