﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Supermodel.DataAnnotations.Validations;
using Supermodel.Persistence.Repository;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.ReflectionMapper;
using WebMVC.ApiControllers;

namespace WebMVC.Models
{
    public class ClassLabelMvcModel : Bs4.MvcModelForEntity<Class>
    {
        #region Overrides
        public override string Label => Name;
        #endregion
        
        #region Properties
        public string Name { get; set; } = "";
        #endregion
    }
    
    public class ClassMvcModel : Bs4.MvcModelForEntity<Class>
    {
        #region Overrides
        public override string Label => Name.Value;
        public override Task MapFromCustomAsync<T>(T other)
        {
            var @class = CastToEntity(other);

            // ReSharper disable once ConstantConditionalAccessQualifier
            Professor.Value = $"{@class.Professor?.FirstName} {@class.Professor?.LastName}";

            return base.MapFromCustomAsync(other);
        }
        public override async Task<T> MapToCustomAsync<T>(T other)
        {
            var @class = CastToEntity(other);

            var professorNameLowerCase = Professor.Value.ToLower().Trim();
            var professor = await LinqRepoFactory.Create<Professor>().Items
                .SingleOrDefaultAsync(x => x.FullName.ToLower() == professorNameLowerCase);

            if (professor == null) 
            {
                throw new ValidationResultException($"Unknown Professor {Professor.Value}", new [] { nameof(Professor) });
            }

            @class.Professor = professor;

            return await base.MapToCustomAsync(other);
        }
        #endregion
        
        #region Properties
        [Required] public Bs4.TextBoxMvcModel Name { get; set; } = new Bs4.TextBoxMvcModel();
        [Required] public Bs4.TextBoxMvcModel Credits { get; set; } = new Bs4.TextBoxMvcModel();

        //public ProfessorDropdown Professor { get; set; } = new ProfessorDropdown { DisabledSuffix = " [FIRED]" };
        [Required, NotRMapped] public Bs4.AutocompleteTextBoxMvcModel Professor { get; set; } = new Bs4.AutocompleteTextBoxMvcModel(typeof(ProfessorLookupApiController));
        public Bs4.CheckboxListMvcModelUsing<StudentLabelMvcModel> StudentsAttended { get; set; } = new Bs4.CheckboxListMvcModelUsing<StudentLabelMvcModel>();
        #endregion
    }
}
