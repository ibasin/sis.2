﻿#nullable enable

using Domain.Entities;
using Supermodel.Persistence.Entities;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;

namespace WebMVC.Models
{
    public class ToDoItemMvcModel : Bs4.MvcModelForEntity<ToDoItem>
    {
        #region Overrides
        public override string Label => Name.Value;
        public override IEntity CreateEntity()
        {
            var entity = (ToDoItem)base.CreateEntity();
            entity.SIS2User = new SIS2User();
            return entity;
        }
        #endregion

        #region Properties
        public Bs4.TextBoxMvcModel Name { get; set; } = new Bs4.TextBoxMvcModel();
        public Bs4.TextBoxMvcModel Priority { get; set; } = new Bs4.TextBoxMvcModel();
        #endregion
    }
}
