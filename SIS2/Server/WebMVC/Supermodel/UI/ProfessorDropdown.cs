﻿#nullable enable

using System.Linq;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Extensions;
using WebMVC.Models;

namespace WebMVC.Supermodel.UI
{
    public class ProfessorDropdown : Bs4.DropdownMvcModelUsing<ProfessorMvcModel> 
    { 
        #region Overrides
        public override IHtmlContent DisplayTemplate<T>(IHtmlHelper<T> html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string? markerAttribute = null)
        {
            var baseDisplay = base.DisplayTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute);
            if (Options.Single(x => x.Value == SelectedValue).IsDisabled)
            {
                var baseDisplayString = baseDisplay.GetString(); 
                return $"{baseDisplayString}{DisabledSuffix}".ToHtmlEncodedHtmlString();
            }
            else
            {
                return baseDisplay;
            }
        }
        #endregion
    }
}
