using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Supermodel.Encryptor;
using Supermodel.Persistence.EFCore;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Auth;

namespace WebMVC.Supermodel.Auth
{
    public class ApiSecureAuthenticationHandler: SupermodelApiAuthenticationHandlerBase
    {
        #region Constructors
        public ApiSecureAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock){}
        #endregion

        #region Overrides
        protected override Task<List<Claim>> AuthenticateBasicAndGetClaimsAsync(string username, string password)
        {
            throw new InvalidOperationException(); 
        }
        protected override async Task<List<Claim>> AuthenticateEncryptedAndGetClaimsAsync(string[] args)
        {
            if (args.Length != 4) return new List<Claim>();

            await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            {
                var utcNow = DateTime.UtcNow;

                var username = args[0];
                var password = args[1];
                var secretTokenHash = args[2];
                var secretTokenHashSalt = args[3];

                var repo = (EFCoreSimpleDataRepo<SIS2User>)RepoFactory.Create<SIS2User>();
                var lowerCaseUsername =  username.ToLower();
                var user = repo.Items.SingleOrDefault(u => u.Username.ToLower() == lowerCaseUsername);

                var secretTokenValid = false;

                if (user != null && !string.IsNullOrEmpty(user.Username))
                {
                    var dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(utcNow.AddMinutes(-5));
                    if (HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt) == secretTokenHash) secretTokenValid = true;

                    dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(utcNow);
                    if (HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt) == secretTokenHash) secretTokenValid = true;

                    dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(utcNow.AddMinutes(5));
                    if (HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt) == secretTokenHash) secretTokenValid = true;
                }

                if (user != null && user.PasswordEquals(password) && secretTokenValid)
                {
                    var claims = AuthClaimsHelper.CreateNewClaimsListWithIdAndLabel(user.Id, $"{user.FirstName} {user.LastName}");
                    
                    //Add claims for the specific permissions following the example below
                    if (user.Admin) claims.Add(new Claim(ClaimTypes.Role, SIS2User.AdminRole, ClaimValueTypes.String));
                    
                    return claims;
                }
                else
                {
                    return new List<Claim>();
                }
            }
        }
        #endregion

        #region Properties
        protected override byte[] EncryptionKey => Key;
        protected override string AuthHeaderName => HeaderName;
        #endregion

        #region Shared Constants
        public static readonly byte[] Key = { 0x94, 0x4F, 0x06, 0xB9, 0xB2, 0x09, 0xAE, 0x1A, 0x16, 0xEF, 0xEC, 0xCE, 0x1E, 0x35, 0x85, 0x8D };
        public static readonly string HeaderName = "X-SIS2-Authorization";
        // ReSharper disable StringLiteralTypo
        public static readonly string SecretToken = "YGCkjQ6qJGdO35QRAiPfq5gbG++wnXtlXB6c9w3MLBRWc5E0RKELjns5ZWAZ+Hzd2e1AFLG0YcoBsRISUcfEQQ";
        // ReSharper restore StringLiteralTypo
        #endregion
    }
}
