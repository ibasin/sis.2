﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using Supermodel.Presentation.Mvc.Extensions.Gateway;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class MajorController 
        : ChildCRUDController<Major, MajorDetailMvcModel, Department, DepartmentController, DataContext> 
    { 
        #region Overrides
        protected override async Task<IActionResult> AfterCreateAsync(long id, long parentId, Major entityItem, MajorDetailMvcModel mvcModelItem)
        {
            await UnitOfWorkContext.FinalSaveChangesAsync();
            TempData.Super().NextPageModalMessage = "Major created successfully!";
            return StayOnDetailScreen(entityItem.Id);
        }
        protected override IQueryable<Major> GetItems()
        {
            return base.GetItems().Include(x => x.Students);
        }
        #endregion
    }
}
