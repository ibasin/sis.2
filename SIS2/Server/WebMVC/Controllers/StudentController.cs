﻿#nullable enable

using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class StudentController 
        : ChildCRUDController<Student, StudentMvcModel, Major, MajorController, DataContext> 
    { 
        #region Overrides
        protected override IQueryable<Student> GetItems()
        {
            return base.GetItems().Include(x => x.ClassesTaken);
        }
        #endregion
    }
}
