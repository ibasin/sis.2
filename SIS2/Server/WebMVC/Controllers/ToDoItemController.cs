﻿#nullable enable

using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Supermodel.Presentation.Mvc.Auth;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class ToDoItemController : CRUDController<ToDoItem, ToDoItemMvcModel, DataContext>
    {
        #region Overrides
        protected override IActionResult StayOnDetailScreen(long id)
        {
            return GoToListScreen();
        }
        protected override IQueryable<ToDoItem> GetItems()
        {
            var userId = UserHelper.GetCurrentUserId();
            return base.GetItems().Where(x => x.SIS2User.Id == userId).OrderBy(x => x.Priority);
        }
        #endregion
    }
}
