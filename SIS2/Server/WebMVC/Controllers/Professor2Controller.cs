﻿#nullable enable

using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Supermodel.Presentation.Mvc.Controllers;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class Professor2Controller : CRUDController<Professor, Professor2MvcModel, DataContext>
    {
        #region Disabled Action Methods
        public override Task<IActionResult> Detail(long id, bool? isInline, HttpPut ignore)
        {
            throw new InvalidOperationException();
        }
        public override Task<IActionResult> Detail(long id, bool? isInline, HttpPost ignore)
        {
            throw new InvalidOperationException();
        }
        public override Task<IActionResult> Detail(long id, HttpDelete ignore)
        {
            throw new InvalidOperationException();
        }
        protected override Task<IActionResult> AfterBinaryDeleteAsync(long id, Professor entityItem, Professor2MvcModel mvcModelItem)
        {
            throw new InvalidOperationException();
        }
        #endregion
        
        #region Overrides
        protected override IQueryable<Professor> GetItems()
        {
            return base.GetItems().Where(x => x.Active);
        }
        #endregion
    }
}
