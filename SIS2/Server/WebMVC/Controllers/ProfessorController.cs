﻿#nullable enable

using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class ProfessorController : CRUDController<Professor, ProfessorMvcModel, DataContext> 
    { 
        #region Overrides
        protected override IQueryable<Professor> GetItems()
        {
            return base.GetItems().Where(x => x.Active);
        }
        #endregion
    }
}
