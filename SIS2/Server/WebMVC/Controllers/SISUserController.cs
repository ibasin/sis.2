﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Bootstrap4.D3.Models;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    [Authorize(Roles = SIS2User.AdminRole)]
    public class SIS2UserController : EnhancedCRUDController<SIS2User, SIS2UserMvcModel, Bs4.SimpleSearchMvcModel, DataContext> 
    { 
        #region Action methods
        [HttpPost]
        public async Task<IActionResult> DeleteToDoItems(long id)
        {
            await using(new UnitOfWork<DataContext>())
            {
                var sis2User = await RepoFactory.Create<SIS2User>().GetByIdAsync(id);
                sis2User.DeleteToDoItems();
            }
            return GoToListScreen();
        }

        public override async Task<IActionResult> List(int? smSkip = null, int? smTake = null, string? smSortBy = null)
        {
            await using(new UnitOfWork<DataContext>(ReadOnly.Yes))
            {
                var sis2Users = await RepoFactory.Create<SIS2User>().GetAllAsync();

                var barChart = new D3.BarChartMvcModel();
                var donutChart = new D3.DonutChartMvcModel();

                foreach (var sis2User in sis2Users)
                {
                    var barDatum = new D3.BarChartMvcModel.Datum(sis2User.Username, sis2User.ToDoItems.Count);
                    barChart.Data.Add(barDatum);

                    var donutDatum = new D3.DonutChartMvcModel.Datum(sis2User.Id, sis2User.Username, sis2User.ToDoItems.Count);
                    donutChart.Data.Add(donutDatum);
                }
                barChart.LabelsNumberFormat = "d";
                barChart.Width = 1500;
                ViewBag.BarChart = barChart;

                ViewBag.DonutChart = donutChart;
            
                return await base.List(smSkip, smTake, smSortBy);
            }
        }
        #endregion
        
        #region Overrides
        protected override IQueryable<SIS2User> ApplySearchBy(IQueryable<SIS2User> items, Bs4.SimpleSearchMvcModel searchBy)
        {
            if (!string.IsNullOrEmpty(searchBy.SearchTerm.Value))
            {
                var searchTermLowerCase = searchBy.SearchTerm.Value.ToLower();
                items = items.Where(x => x.FirstName.ToLower().Contains(searchTermLowerCase) ||
                                         x.LastName.ToLower().Contains(searchTermLowerCase) ||
                                         x.Username.ToLower().Contains(searchTermLowerCase));
            }
            return items;
        }
        protected override IOrderedQueryable<SIS2User> ApplySortBy(IQueryable<SIS2User> items, string? sortBy)
        {
            if (sortBy == "ToDoItem.Count") return items.OrderBy(x => x.ToDoItems.Count);
            if (sortBy == "-ToDoItem.Count") return items.OrderByDescending(x => x.ToDoItems.Count);
            
            if (sortBy == "Address") return items.OrderBy(x => x.Address.Zip);
            if (sortBy == "-Address") return items.OrderByDescending(x => x.Address.Zip);

            return base.ApplySortBy(items, sortBy);
        }
        protected override IQueryable<SIS2User> GetItems()
        {
            return base.GetItems().Include(x => x.SIS2UserPicture);
        }
        protected override IActionResult StayOnDetailScreen(long id)
        {
            return GoToListScreen();
        }
        #endregion
    }
}
