﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using Supermodel.Presentation.Mvc.Extensions.Gateway;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class DepartmentController : EnhancedCRUDController<Department, DepartmentDetailMvcModel, DepartmentListMvcModel, Bs4.DummySearchMvcModel, DataContext> 
    { 
        #region Overrides
        protected override async Task<IActionResult> AfterCreateAsync(long id, Department entityItem, DepartmentDetailMvcModel mvcModelItem)
        {
            await UnitOfWorkContext.FinalSaveChangesAsync();
            TempData.Super().NextPageModalMessage = "Department created successfully!";
            return StayOnDetailScreen(entityItem.Id);
        }
        protected override IQueryable<Department> GetItems()
        {
            return base.GetItems().Include(x => x.Majors);
        }
        #endregion
    }
}
