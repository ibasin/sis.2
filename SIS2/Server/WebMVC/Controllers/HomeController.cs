﻿#nullable enable

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebMVC.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("List", "ToDoItem");

            //var user = await UserHelper.GetCurrentUserAsync<SIS2User, DataContext>();
            //if (user == null) throw new NoNullAllowedException("(user == null)");
            //var url = Bs4.Message.RegisterSingleReadMessageAndGetUrl($"{user.FirstName}, welcome to SIS. Today is {DateTime.Today.ToShortDateString()}");
            //return LocalRedirect(url);
        }
    }
}
