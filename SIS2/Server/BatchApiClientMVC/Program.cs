#nullable enable

using System;
using System.Threading.Tasks;
using BatchApiClientMVC.Supermodel.Auth;
using BatchApiClientMVC.Supermodel.Persistence;
using Supermodel.ApiClient.Models;
using Supermodel.Mobile.Runtime.Common.UnitOfWork;

namespace BatchApiClientMVC
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();

            //var authHeaderGenerator = new BasicAuthHeaderGenerator("supermodel@noblis.org", "1234");
            var authHeaderGenerator = new SIS2SecureAuthHeaderGenerator("supermodel@noblis.org", "1234", new byte[0]);

            //await using (new UnitOfWork<SIS2WebApiDataContext>())
            //{
            //    UnitOfWorkContext.AuthHeader = authHeaderGenerator.CreateAuthHeader();

            //    var user = new SIS2UserUpdatePassword
            //    {
            //        Id = 1,
            //        OldPassword = "1234",
            //        NewPassword = "12345"
            //    };
            //    user.Update(); //we do update because we need to "attach" the model, make it managed
            //}
            //Console.WriteLine("User with id=1's password updated to '12345'");

            //await using (new UnitOfWork<SIS2WebApiDataContext>())
            //{
            //    var repo = RepoFactory.Create<Department>();
            //    // ReSharper disable once UnusedVariable
            //    var departments = await repo.GetAllAsync();
            //    departments[0].Name += " Updated!";
            //    //departments[1].Delete();

            //    var newDepartment = new Department { Name = "New Department" };
            //    newDepartment.Add();

            //    //UnitOfWorkContext.CommitOnDispose = false;
            //}

            //Console.WriteLine("Press enter");
            //Console.ReadLine();

            //// ReSharper disable once RedundantAssignment
            //var delayedAllDepartments = new DelayedModels<Department>();
            //await using (new UnitOfWork<SIS2WebApiDataContext>())
            //{
            //    var repo = RepoFactory.Create<Department>();
            //    repo.DelayedGetAll(out delayedAllDepartments);

            //    var newDepartment = (await repo.GetWhereAsync(new SimpleSearch { SearchTerm = "New Department" })).SingleOrDefault();
            //    if (newDepartment != null) newDepartment.Delete();
            //}
            //Console.WriteLine(delayedAllDepartments.Values.Count);

            //await using (new UnitOfWork<SIS2WebApiDataContext>())
            //{
            //    UnitOfWorkContext.AuthHeader = authHeaderGenerator.CreateAuthHeader();
                
            //    var input = new Input { Suffix = " [REACTIVATED]" };
            //    var output = await UnitOfWorkContext<SIS2WebApiDataContext>.CurrentDataContext.ReactivateProfessorsApiAsync(input);
            //    Console.WriteLine($"Reactivated {output.ReactivatedCount} professors...");
            //}

            await using (new UnitOfWork<SIS2WebApiDataContext>())
            {
                UnitOfWorkContext.AuthHeader = authHeaderGenerator.CreateAuthHeader();

                var result = await UnitOfWorkContext<SIS2WebApiDataContext>.CurrentDataContext.ValidateLoginAsync<Department>();
                if (result.LoginSuccessful)
                {
                    Console.WriteLine($"Welcome, {result.UserLabel}");
                }
                else
                {
                    Console.WriteLine($"Login failed");
                }
            }
        }
    }
}
