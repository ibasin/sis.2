#nullable enable

using System;
using System.Diagnostics;
using System.Net.Http;
using Supermodel.Mobile.Runtime.Common.DataContext.WebApi;

namespace BatchApiClientMVC.Supermodel.Persistence
{
    public class SIS2WebApiDataContext: WebApiDataContext
    {
        #region Overrides
        public override string BaseUrl => "http://localhost:58158/"; //this one is for MVC

        // set timeout to 10 min, so we can debug
        protected override HttpClient CreateHttpClient()
        {
            var httpClient = base.CreateHttpClient();
            if (Debugger.IsAttached) httpClient.Timeout = new TimeSpan(0, 10, 0);
            return httpClient;
        }
        #endregion
    }
}
