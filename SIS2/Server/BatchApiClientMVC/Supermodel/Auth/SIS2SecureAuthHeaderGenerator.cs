#nullable enable

using System;
using Supermodel.Encryptor;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.Login;

namespace BatchApiClientMVC.Supermodel.Auth
{
    public class SIS2SecureAuthHeaderGenerator : BasicAuthHeaderGenerator
    {
        #region Constructors
        public SIS2SecureAuthHeaderGenerator(string username, string password, byte[] localStorageEncryptionKey) : base(username, password, localStorageEncryptionKey){}
        #endregion

        #region Overrdies
        public override AuthHeader CreateAuthHeader()
        {
            var dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(DateTime.UtcNow);
            var secretTokenHashSalt = HashAgent.GenerateGuidSalt();
            var secretTokenHash = HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt);
            var authHeader = HttpAuthAgent.CreateSMCustomEncryptedAuthHeader(Key, Username, Password, secretTokenHash, secretTokenHashSalt);
            authHeader.HeaderName = HeaderName;
            return authHeader;
        }
        #endregion

        #region Shared Constants
        public static readonly byte[] Key = { 0x94, 0x4F, 0x06, 0xB9, 0xB2, 0x09, 0xAE, 0x1A, 0x16, 0xEF, 0xEC, 0xCE, 0x1E, 0x35, 0x85, 0x8D };
        public static readonly string HeaderName = "X-SIS2-Authorization";
        // ReSharper disable StringLiteralTypo
        public static readonly string SecretToken = "YGCkjQ6qJGdO35QRAiPfq5gbG++wnXtlXB6c9w3MLBRWc5E0RKELjns5ZWAZ+Hzd2e1AFLG0YcoBsRISUcfEQQ";
        // ReSharper restore StringLiteralTypo
        #endregion
    }
}
