//   DO NOT MAKE CHANGES TO THIS FILE. THEY MIGHT GET OVERWRITTEN!!!
//   Auto-generated by Supermodel.Mobile on 11/20/2020 3:57:24 PM
//

// ReSharper disable RedundantUsingDirective
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Supermodel.Mobile.Runtime.Common.DataContext.WebApi;
using Supermodel.Mobile.Runtime.Common.Models;
using System.ComponentModel;
using Supermodel.DataAnnotations.Validations.Attributes;
// ReSharper restore RedundantUsingDirective

// ReSharper disable once CheckNamespace
namespace Supermodel.ApiClient.Models
{
	#region DepartmentApiController
	[RestUrl("DepartmentApi")]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class Department : Model
	{
		#region Properties
		public String Name { get; set; }
		public List<Major> Majors { get; set; } = new List<Major>();
		#endregion
	}
	
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class SimpleSearch
	{
		#region Properties
		public String SearchTerm { get; set; }
		#endregion
	}
	#endregion
	
	#region SIS2UserUpdatePasswordApiController
	[RestUrl("SIS2UserUpdatePasswordApi")]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class SIS2UserUpdatePassword : Model
	{
		#region Properties
		public String OldPassword { get; set; }
		public String NewPassword { get; set; }
		#endregion
	}
	#endregion
	
	#region ReactivateProfessorsApiController
	//Extension method for ReactivateProfessorsApi command
	public static class ReactivateProfessorsApiCommandExt
	{
		public static async Task<Output> ReactivateProfessorsApiAsync(this WebApiDataContext me, Input input)
		{
			return await me.ExecutePostAsync<Input, Output>("ReactivateProfessorsApi", input);
		}
	}
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class Input
	{
		#region Properties
		public String Suffix { get; set; }
		#endregion
	}
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class Output
	{
		#region Properties
		public Int32 ReactivatedCount { get; set; }
		#endregion
	}
	#endregion
	
	#region Types models depend on and types that were specifically marked with [IncludeInApiClient]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class Major
	{
		#region Properties
		public String Name { get; set; }
		public DateTime? DateCreated { get; set; }
		public DateTime? DateModified { get; set; }
		public List<Student> Students { get; set; } = new List<Student>();
		public Int64 Id { get; set; }
		#endregion
	}
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class Student
	{
		#region Properties
		public String SocialSecurityNumber { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public GenderEnum Gender { get; set; }
		public USAddress HomeAddress { get; set; }
		public USAddress SchoolAddress { get; set; }
		public Int64 Id { get; set; }
		#endregion
	}
	public enum GenderEnum
	{
		Male = 0,
		Female = 1,
		[Description("Non-binary")] NonBinary = 2
	}
	
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class USAddress
	{
		#region Properties
		public String Street { get; set; }
		public String City { get; set; }
		public String State { get; set; }
		public String Zip { get; set; }
		#endregion
	}
	#endregion
}
