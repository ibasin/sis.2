#nullable enable

using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Supermodel.Persistence
{
    public static class DataSeeder
    {
        #region Methods
        public static Task SeedDataAsync()
        {
            var users = new[] 
            { 
                new SIS2User { FirstName = "Sample", LastName = "Account", Username="supermodel@noblis.org", Password="1234", Admin = true },
                new SIS2User { FirstName = "Ilya", LastName = "Basin", Username="ilya.basin@noblis.org", Password="1234" },
                new SIS2User { FirstName = "Shiloh", LastName = "Enriquez", Username="Shiloh.Enriquez@noblis.org", Password="1234" },
                new SIS2User { FirstName = "Surendra", LastName = "Upadhaya", Username="Surendra.Upadhaya@noblis.org", Password="1234" },
                new SIS2User { FirstName = "Shabbir", LastName = "Mohammad", Username="Shabbir.Mohammad@noblis.org", Password="1234" },
                new SIS2User { FirstName = "Carrie", LastName = "Song", Username="Carrie.Song@noblis.org", Password="1234" },
            };
            foreach (var user in users) user.Add();

            users[0].ToDoItems.Add(new ToDoItem { Name = "File annual report" });
            users[0].ToDoItems.Add(new ToDoItem { Name = "Approve timesheet" });
            users[0].ToDoItems.Add(new ToDoItem { Name = "Fire annoying employees" });

            users[1].ToDoItems.Add(new ToDoItem { Name = "Prepare for the presentation" });

            var mathMajor = new Major { Name = "Math" };
            mathMajor.Add();

            var englishMajor = new Major { Name = "English" };
            englishMajor.Add();

            var csMajor = new Major { Name = "Computer Science" };
            csMajor.Add();

            var departments = new []
            {
                new Department{ Name = "Languages", Majors = { englishMajor} },
                new Department{ Name = "STEM", Majors = { mathMajor, csMajor} },
            };
            foreach (var department in departments) department.Add();
            
            var students = new []
            {
                new Student { SocialSecurityNumber = "111-11-1111", FirstName = "Jerry", LastName = "Seinfeld", Gender = GenderEnum.Male, Major = mathMajor }, 
                new Student { SocialSecurityNumber = "222-22-2222",FirstName = "George", LastName = "Costanza", Gender = GenderEnum.Male, Major = englishMajor }, 
                new Student { SocialSecurityNumber = "333-33-3333",FirstName = "Cosmo", LastName = "Karmer", Gender = GenderEnum.NonBinary, Major = csMajor }, 
                new Student { SocialSecurityNumber = "444-44-4444",FirstName = "Elane", LastName = "Bens", Gender = GenderEnum.Female, Major = englishMajor }, 
            };
            foreach (var student in students) student.Add();

            var feynmanProfessor = new Professor { FirstName = "Richard", LastName = "Feynman", Gender = GenderEnum.Male, SocialSecurityNumber = "999-99-9999" };
            feynmanProfessor.Add();

            var vonnegutProfessor = new Professor { FirstName = "Kurt", LastName = "Vonnegut", Gender = GenderEnum.Male, SocialSecurityNumber = "888-88-8888" };
            vonnegutProfessor.Add();

            var classes = new[]
            {
                new Class() { Name = "English 101", Credits = 3, Professor = vonnegutProfessor },
                new Class() { Name = "CS 110", Credits = 4, Professor = feynmanProfessor },
                new Class() { Name = "Chemistry 110", Credits = 4, Professor = feynmanProfessor }
            };
            foreach (var @class in classes) @class.Add();

            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[0]});
            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[1]});
            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[2]});
            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[3]});

            return Task.CompletedTask;
        }
        #endregion
    }
}
