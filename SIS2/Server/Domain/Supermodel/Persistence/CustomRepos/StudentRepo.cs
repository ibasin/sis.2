﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Supermodel.Persistence.EFCore;

namespace Domain.Supermodel.Persistence.CustomRepos
{
    public class StudentRepo : EFCoreSimpleDataRepo<Student>
    {
        public Task<List<Student>> GetByGenderAsync(GenderEnum gender)
        {
            return Items.Where(x => x.Gender == gender).ToListAsync();
        }
    }
}
