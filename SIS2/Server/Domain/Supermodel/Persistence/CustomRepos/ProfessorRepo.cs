﻿#nullable enable

using System.Linq;
using Domain.Entities;
using Supermodel.Persistence.EFCore;

namespace Domain.Supermodel.Persistence.CustomRepos
{
    public class ProfessorRepo : EFCoreSimpleDataRepo<Professor>
    {
        public override IQueryable<Professor> Items => base.Items.Where(x => x.Active);
        public override void Delete(Professor item) => item.Active = false;

        public IQueryable<Professor> AllItems => base.Items;
        public IQueryable<Professor> DeletedItems => base.Items.Where(x => !x.Active);
    }
}
