﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.DataAnnotations.Validations;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Entities.ValueTypes;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;

namespace Domain.Entities
{
    public class Student : Person
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            foreach (var classTaken in ClassesTaken) classTaken.Delete();
            base.DeleteInternal();
        }
        public override async Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            var vrl = await base.ValidateAsync(validationContext);

            await using(new UnitOfWork<DataContext>())
            {
                var repo = LinqRepoFactory.Create<Student>(); 
                if (await repo.Items.AnyAsync(x => x.SocialSecurityNumber == SocialSecurityNumber && x.Id != Id))
                {
                    vrl.AddValidationResult(this, "User with this social security number already exists", x => x.SocialSecurityNumber);
                }
            }

            return vrl;
        }
        #endregion
        
        #region Properties
        public virtual USAddress SchoolAddress { get; set; } = new USAddress();

        [Required] public virtual Major Major { get; set; } = default!;

        public virtual M2MList<M2MStudentClass> ClassesTaken { get; set; } = new M2MList<M2MStudentClass>();
        #endregion
    }

    public class StudentModelConfig : IEntityTypeConfiguration<Student>
    {
        #region Methods
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.HasIndex(x => x.SocialSecurityNumber).IsUnique();
            builder.HasIndex(x => x.Gender);
            builder.HasIndex(x => new { x.LastName, x.FirstName });
        }
        #endregion
    }
}
