﻿#nullable enable

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.DataAnnotations.Validations;
using Supermodel.Persistence.DataContext;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;

namespace Domain.Entities
{
    public class Professor : Person
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            Active = false;
        }
        public override async Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            var vrl = await base.ValidateAsync(validationContext);

            await using(new UnitOfWork<DataContext>())
            {
                var repo = LinqRepoFactory.Create<Student>(); 
                if (await repo.Items.AnyAsync(x => x.SocialSecurityNumber == SocialSecurityNumber && x.Id != Id))
                {
                    vrl.AddValidationResult(this, "User with this social security number already exists", x => x.SocialSecurityNumber);
                }
                if (await repo.Items.AnyAsync(x => x.LastName == LastName && x.FirstName == FirstName && x.Id != Id))
                {
                    vrl.AddValidationResult(this, "Last Name / First Name combination must be unique", x => x.LastName, x => x.FirstName);
                }
            }

            return vrl;
        }
        public override void BeforeSave(OperationEnum operation)
        {
            FullName = $"{FirstName} {LastName}";
            base.BeforeSave(operation);
        }
        #endregion

        #region Properties
        public bool Active { get; set; } = true;
        public virtual List<Class> Classes {get; set;} = new List<Class>();

        public string FullName { get; set; } = "";
        #endregion
    }

    public class ProfessorModelConfig : IEntityTypeConfiguration<Professor>
    {
        #region Methods
        public void Configure(EntityTypeBuilder<Professor> builder)
        {
            builder.HasIndex(x => x.SocialSecurityNumber).IsUnique();
            builder.HasIndex(x => x.Gender);
            builder.HasIndex(x => new { x.LastName, x.FirstName }).IsUnique();
            builder.HasIndex(x => x.FullName).IsUnique();

            builder.HasIndex(x => x.Active);
        }
        #endregion
    }
}
