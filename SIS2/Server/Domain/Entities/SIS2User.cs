#nullable enable

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Entities.ValueTypes;

namespace Domain.Entities
{
    public class SIS2User : UserEntity<SIS2User, DataContext>
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            SIS2UserPicture.Delete();
            DeleteToDoItems();

            base.DeleteInternal();
        }
        #endregion

        #region Methods
        public void DeleteToDoItems()
        {
            foreach (var toDoItem in ToDoItems) toDoItem.Delete();
        }
        #endregion
        
        #region Constants
        public const string AdminRole = "Admin";
        #endregion
        
        #region Properties
        [Required] public string FirstName { get; set; } = "";
        [Required] public string LastName { get; set; } = "";

        public virtual USAddress Address { get; set; } = new USAddress();

        public bool Admin { get; set; }

        [Required] public virtual SIS2UserPicture SIS2UserPicture { get; set; } = new SIS2UserPicture();

        public virtual List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
        #endregion
    }

    public class SIS2UserModelConfig : IEntityTypeConfiguration<SIS2User>
    {
        #region Methods
        public void Configure(EntityTypeBuilder<SIS2User> builder)
        {
            builder.HasOne(a => a.SIS2UserPicture).WithOne(b => b.SIS2User).HasForeignKey<SIS2UserPicture>();
        }
        #endregion
    }
}
