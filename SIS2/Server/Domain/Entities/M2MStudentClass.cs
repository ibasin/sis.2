﻿#nullable enable

using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class M2MStudentClass : M2M<Student, Class> { }
}
