﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class ToDoItem : Entity
    {
        #region Properties
        public long SIS2UserId { get; set; }
        [Required] public virtual SIS2User SIS2User { get; set; } = default!;

        [Required] public string Name { get; set; } = "";
        public int Priority { get; set; } = 10;
        #endregion
    }

    public class ToDoItemModelConfig : IEntityTypeConfiguration<ToDoItem>
    {
        #region Methods
        public void Configure(EntityTypeBuilder<ToDoItem> builder)
        {
            builder.HasIndex(x => new { x.SIS2UserId, x.Priority });
        }
        #endregion
    }
}
