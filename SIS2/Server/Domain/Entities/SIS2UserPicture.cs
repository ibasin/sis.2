﻿#nullable enable

using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Supermodel.DataAnnotations.Validations;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Entities.ValueTypes;

namespace Domain.Entities
{
    public class SIS2UserPicture : Entity
    {
        #region Overrides
        public override async Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            var vrl =  await base.ValidateAsync(validationContext);

            if (!Picture.IsEmpty && 
                !Picture.Extension.Equals(".jpg", StringComparison.InvariantCultureIgnoreCase) && 
                !Picture.Extension.Equals(".jpeg", StringComparison.InvariantCultureIgnoreCase) &&
                !Picture.Extension.Equals(".gif", StringComparison.InvariantCultureIgnoreCase) &&
                !Picture.Extension.Equals(".png", StringComparison.InvariantCultureIgnoreCase))
                vrl.AddValidationResult(this, "Invalid image file type", x => x.Picture);

            return vrl;
        }
        #endregion
        
        #region Properties
        public virtual BinaryFile Picture { get; set; } = new BinaryFile();

        [Required] public virtual SIS2User SIS2User { get; set; } = default!;
        #endregion
    }
}
