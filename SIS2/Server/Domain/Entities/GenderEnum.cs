﻿#nullable enable

using System.ComponentModel;

namespace Domain.Entities
{
    public enum GenderEnum
    {
        Male,
        Female,
        [Description("Non-binary")] NonBinary
    }
}
