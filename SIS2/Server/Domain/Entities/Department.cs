﻿#nullable enable

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Supermodel.Persistence;
using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class Department : Entity
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            if (Majors.Any()) throw new UnableToDeleteException("Department contains Majors, cannot be deleted");
            base.DeleteInternal();
        }
        #endregion
        
        #region Properties
        [Required] public string Name { get; set; } = "";

        public virtual List<Major> Majors { get; set; } = new List<Major>();
        #endregion
    }
}
