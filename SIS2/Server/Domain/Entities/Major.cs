﻿#nullable enable

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.DataAnnotations.Validations;
using Supermodel.Persistence;
using Supermodel.Persistence.DataContext;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;

namespace Domain.Entities
{
    public class Major : Entity
    {
        #region Overrides
        public override async Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            var vrl = await base.ValidateAsync(validationContext);

            await using(new UnitOfWork<DataContext>())
            {
                var repo = LinqRepoFactory.Create<Major>(); 
                if (await repo.Items.AnyAsync(x => x.Name == Name && x.Id != Id))
                {
                    vrl.AddValidationResult(this, "Major Name must be unique", x => x.Name);
                }
            }

            return vrl;
        }
        protected override void DeleteInternal()
        {
            if (Students.Any()) throw new UnableToDeleteException("Can not delete majors with associated students");
            //foreach(var student in Students)
            //{
            //    student.Delete();
            //}
            base.DeleteInternal();
        }
        public override void BeforeSave(OperationEnum operation)
        {
            if (operation == OperationEnum.Add) DateCreated = DateModified = DateTime.Now;
            if (operation == OperationEnum.Update) DateModified = DateTime.Now;
            base.BeforeSave(operation);
        }
        #endregion
        
        #region Properties
        [Required] public virtual Department Department { get; set; } = default!;
        
        [Required] public string Name { get; set; } = "";
        [Required] public DateTime? DateCreated { get; set; }
        [Required] public DateTime? DateModified { get; set; }

        public virtual List<Student> Students { get; set; } = new List<Student>();
        #endregion
    }

    public class MajorModelConfig : IEntityTypeConfiguration<Major>
    {
        #region Methods
        public void Configure(EntityTypeBuilder<Major> builder)
        {
            builder.HasIndex(x => x.Name).IsUnique();
        }
        #endregion
    }
}
