﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Entities.ValueTypes;

namespace Domain.Entities
{
    public abstract class Person : Entity
    {
        #region Properties
        [Required] public string SocialSecurityNumber { get; set; } = "";
        [Required] public string FirstName { get; set; } = "";
        [Required] public string LastName { get; set; } = "";
        public GenderEnum Gender { get; set; }

        public virtual USAddress HomeAddress { get; set; } = new USAddress();
        #endregion
    }
}
