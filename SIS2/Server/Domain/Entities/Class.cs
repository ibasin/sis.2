﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class Class : Entity
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            foreach (var studentAttended in StudentsAttended) studentAttended.Delete();
            base.DeleteInternal();
        }
        #endregion
        
        #region Properties
        [Required] public virtual Professor Professor { get; set; } = default!;
        
        [Required] public string Name { get; set; } = "";
        public int Credits { get; set; }

        public virtual M2MList<M2MStudentClass> StudentsAttended { get; set; } = new M2MList<M2MStudentClass>();
        #endregion
    }
}
