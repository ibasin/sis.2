using Foundation;
using Supermodel.Mobile.Runtime.iOS.App;
using SIS2.Mobile.AppCore;

namespace SIS2.Mobile.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : iOSFormsApplication<SIS2App> { }
}