using Android.App;
using SIS2.Mobile.AppCore;
using Supermodel.Mobile.Runtime.Droid.App;

namespace SIS2.Mobile.Droid
{
    [Activity(Label = "SIS2.Droid", MainLauncher = true)]
    public class MainActivity : DroidFormsApplication<SIS2App> { }
}

