#nullable enable

using System.Threading.Tasks;
using Supermodel.ApiClient.Models;
using Supermodel.Mobile.Runtime.Common.UnitOfWork;
using Supermodel.Mobile.Runtime.Common.XForms.App;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.CRUDDetail;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.Login;
using SIS2.Mobile.AppCore;
using SIS2.Mobile.Models;
using SIS2.Mobile.Supermodel.Persistence;

namespace SIS2.Mobile.Pages.ChangePassword
{
    public class ChangePasswordPage : CRUDDetailPage<SIS2UserUpdatePassword, SIS2UserUpdatePasswordXFModel, SIS2WebApiDataContext>
    {
        #region Overrides
        protected override async Task SaveItemInternalAsync(SIS2UserUpdatePassword model)
        {
            if (!string.IsNullOrEmpty(model.OldPassword) && !string.IsNullOrEmpty(model.NewPassword))
            {
                await using (SIS2App.RunningApp.NewUnitOfWork<SIS2WebApiDataContext>())
                {
                    model.Update();
                    await UnitOfWorkContext.FinalSaveChangesAsync();
                    var basicAuthHeader = (BasicAuthHeaderGenerator)FormsApplication<SIS2App>.RunningApp.AuthHeaderGenerator;
                    basicAuthHeader.Password = model.NewPassword;
                    await basicAuthHeader.SaveToAppPropertiesAsync();
                }
            }
        }
        #endregion
    }
}
