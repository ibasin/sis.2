#nullable enable

using System;
using System.Collections.ObjectModel;
using Supermodel.ApiClient.Models;
using Supermodel.Mobile.Runtime.Common.XForms.App;
using Supermodel.Mobile.Runtime.Common.XForms.Views;
using Xamarin.Forms;
using SIS2.Mobile.AppCore;
using SIS2.Mobile.Pages.ChangePassword;
//using SIS2UserUpdatePassword = SIS2.Mobile.Models.SIS2UserUpdatePassword;

namespace SIS2.Mobile.Pages.Home
{
    public class HomePage : ContentPage
    {
        #region Constructors
        public HomePage()
        {
            var section = new TableSection();
            var cell = new TextCell { Text = "Change Password >" };
            
            #pragma warning disable CS0618 // Type or member is obsolete
            #pragma warning disable 612
            if (Device.OS == TargetPlatform.iOS) cell.TextColor = Color.FromHex("#007AFF");
            #pragma warning restore 612
            #pragma warning restore CS0618 // Type or member is obsolete

            cell.Tapped += async (sender, args) =>
            {
                if (!FormsApplication<SIS2App>.RunningApp.AuthHeaderGenerator.UserId.HasValue) throw new Exception("AuthHeaderGenerator.UserId must have value");
                var model = new SIS2UserUpdatePassword { Id = FormsApplication<SIS2App>.RunningApp.AuthHeaderGenerator.UserId.Value };
                await FormsApplication<SIS2App>.RunningApp.LoginPage.Navigation.PushAsync(await new ChangePasswordPage().InitAsync(new ObservableCollection<SIS2UserUpdatePassword> { model }, "Change Password", model));
            };
            section.Add(cell);

            Content = new ViewWithActivityIndicator<TableView>(new TableView { Intent = TableIntent.Form, HasUnevenRows = true, Root = new TableRoot{ section } });
        }
        #endregion
    }
}
