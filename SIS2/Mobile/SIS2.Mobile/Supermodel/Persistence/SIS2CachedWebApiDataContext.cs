#nullable enable

using Supermodel.Mobile.Runtime.Common.DataContext.CachedWebApi;

namespace SIS2.Mobile.Supermodel.Persistence
{
    public class SIS2CachedWebApiDataContext : CachedWebApiDataContext<SIS2WebApiDataContext, SIS2SqliteDataContext> {}
}
