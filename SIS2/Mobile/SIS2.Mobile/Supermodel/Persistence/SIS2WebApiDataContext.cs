#nullable enable

using Supermodel.Mobile.Runtime.Common.DataContext.WebApi;

namespace SIS2.Mobile.Supermodel.Persistence
{
    public class SIS2WebApiDataContext: WebApiDataContext
    {
        #region Overrides
        public override string BaseUrl => "http://10.211.55.9:58158/";
        

        // set timeout to 10 min, so we can debug
        //protected override HttpClient CreateHttpClient()
        //{
        //    var httpClient = base.CreateHttpClient();
        //    httpClient.Timeout = new TimeSpan(0, 10, 0);
        //    return httpClient;
        //}
        #endregion
    }
}
