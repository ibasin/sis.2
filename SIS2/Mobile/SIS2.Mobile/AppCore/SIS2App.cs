#nullable enable

using Supermodel.Mobile.Runtime.Common.XForms;
using Supermodel.Mobile.Runtime.Common.XForms.App;
using Xamarin.Forms;
using SIS2.Mobile.Pages.ChangePassword;
using SIS2.Mobile.Pages.Home;
using SIS2.Mobile.Pages.Login;

namespace SIS2.Mobile.AppCore
{
    public class SIS2App : SupermodelXamarinFormsApp
    {
        #region Constructors
        public SIS2App()
        {
            XFormsSettings.LabelFontSize = XFormsSettings.ValueFontSize = 18;

            LoginPage = new LoginPage();
            MainPage = new NavigationPage(LoginPage);

            #pragma warning disable 4014
            LoginPage.AutoLoginIfPossibleAsync();
            #pragma warning restore 4014

            //Init these properties here because we are in #nullable enable context
            HomePage = new HomePage();
            ChangePasswordPage = new ChangePasswordPage();
        }
        #endregion

        #region Overrides
        public override void HandleUnauthorized()
        {
            LoginPage = new LoginPage();
            MainPage = new NavigationPage(FormsApplication<SIS2App>.RunningApp.LoginPage);
        }

        public override byte[] LocalStorageEncryptionKey { get; } = { 0xD6, 0xBE, 0x4D, 0x0A, 0xF6, 0x33, 0x7A, 0xAA, 0x45, 0x68, 0xD1, 0x26, 0x21, 0x60, 0xD5, 0x3A };
        #endregion

        #region Methods
        public static SIS2App RunningApp => FormsApplication<SIS2App>.RunningApp;
        #endregion

        #region Properties
        public LoginPage LoginPage { get; set; }
        public HomePage HomePage { get; set; }
        public ChangePasswordPage ChangePasswordPage { get; set; }
        #endregion
    }
}
